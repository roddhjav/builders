[<img src="https://gitlab.com/uploads/-/system/project/avatar/44709978/logo.png" align="right" height="110"/>][project]

# Builders [![][build]][project] [![][automated]][registry]

**OCI Container images for building packages**


## Description

A set of containers to build packages on a Gitlab pipelines for differents
projects on a large set of Linux distribution.


## How to Build

This image is built on Gitlab automatically any time any time a commit is
made or merged to the `main` branch. It is also build every months. But if you
need to build the image on your own locally, do the following:

  1. [Install Docker][docker].
  2. `cd` into this directory.
  3. Run: `docker build -t registry.gitlab.com/roddhjav/builders/<image> <image>/`


## How to Use

  1. [Install Docker][docker].
  2. Pull this image from the registry. Here `debian`:
  ```sh
  docker pull registry.gitlab.com/roddhjav/builders/debian
  ```
  3. Run a container from the image:
  ```sh
  docker run --detach --name builder-<name> --volume $PWD:/home/build/tmp \
          registry.gitlab.com/roddhjav/builders/debian
  ```

## Gitlab CI

Please see the [apparmor.d] project to see real life example in Gitlab-CI

[project]: https://gitlab.com/roddhjav/builders
[build]: https://gitlab.com/roddhjav/builders/badges/main/pipeline.svg?style=flat-square
[automated]: https://img.shields.io/badge/docker%20build-automated-blue.svg?style=flat-square
[registry]: https://gitlab.com/roddhjav/builders/container_registry

[docker]: https://docs.docker.com/engine/installation/
[apparmor.d]: https://gitlab.com/roddhjav/apparmor.d/-/blob/main/.gitlab-ci.yml
